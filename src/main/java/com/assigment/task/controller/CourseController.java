package com.assigment.task.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assigment.task.model.Course;
import com.assigment.task.model.ValidationMessage;
import com.assigment.task.service.CourseService;
import com.assigment.task.utils.ValidationMessageUtils;

@RestController
@RequestMapping("/api/courses")
public class CourseController {

	@Autowired
	private CourseService courseService;
	
	@GetMapping
	public ResponseEntity<List<Course>> listCourses() {
		List<Course> courses = courseService.listCourses();
		if(courses != null && !courses.isEmpty())
			return new ResponseEntity<>(courses, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@PostMapping
	public ResponseEntity<ValidationMessage> saveCourse(@Valid @RequestBody Course course) {
		courseService.saveCourse(course);
		return new ResponseEntity<>(ValidationMessageUtils.addSuccessMessage("Course created successfully.", "102"), HttpStatus.OK);
	}
	
}
