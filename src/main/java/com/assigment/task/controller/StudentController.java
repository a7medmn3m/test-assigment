package com.assigment.task.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assigment.task.dto.LoginDTO;
import com.assigment.task.model.Course;
import com.assigment.task.model.Student;
import com.assigment.task.model.ValidationMessage;
import com.assigment.task.service.CourseService;
import com.assigment.task.service.StudentService;
import com.assigment.task.utils.ValidationMessageUtils;

@RestController
@RequestMapping("/api/students")
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	@Autowired
	private CourseService courseService;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@PostMapping("/login")
	public ResponseEntity<ValidationMessage> login(@RequestBody LoginDTO loginDTO) {
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(loginDTO.getEmail(), loginDTO.getPassword());
		Authentication authenticate = authenticationManager.authenticate(token);
		if(authenticate == null) 
			return new ResponseEntity<>(ValidationMessageUtils.addErrorMessage("email/password incorrect", "504"), HttpStatus.INTERNAL_SERVER_ERROR);
		return new ResponseEntity<>(ValidationMessageUtils.addSuccessMessage("Loggedin success!", "104"), HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<ValidationMessage> saveStudent(@Valid @RequestBody Student student) {
		Student founded = studentService.getStudent(student.getUsername(), student.getEmail());
		if(founded != null)
			return new ResponseEntity<>(ValidationMessageUtils.addErrorMessage("Email/Username Already Exists", "502"), HttpStatus.OK);
		studentService.saveStudent(student);
		return new ResponseEntity<>(ValidationMessageUtils.addSuccessMessage("Student Saved Successfully", "101"), HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<List<Student>> listAllStudents() {
		List<Student> students = studentService.listStudents();
		if(students != null && !students.isEmpty()) 
			return new ResponseEntity<>(students, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/{id}/courses")
	public ResponseEntity<List<Course>> listStudentCourses(@PathVariable("id") Long id) {
		List<Course> studentCourses = courseService.getStudentCourses(id);
		if(studentCourses == null || studentCourses.isEmpty())
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<>(studentCourses, HttpStatus.OK);
	}
	
	@PostMapping("/{studentId}/regesiter/courses/{courseId}")
	public ResponseEntity<ValidationMessage> registerCourse(@PathVariable("studentId") Long studentId, @PathVariable("courseId") Long courseId) {
		Student student = studentService.getStudent(studentId);
		if(student == null)
			return new ResponseEntity<>(ValidationMessageUtils.addErrorMessage("student with the specified id not found in the database", "507"), HttpStatus.OK);
		Course course = courseService.getCourse(courseId);
		if(course == null)
			return new ResponseEntity<>(ValidationMessageUtils.addErrorMessage("course with the specified id not found in the database", "507"), HttpStatus.OK);
		if(student.getCourses().contains(course)) 
			return new ResponseEntity<>(ValidationMessageUtils.addErrorMessage("Course already registerd for this student", "506"), HttpStatus.OK);
		student.getCourses().add(course);
		studentService.saveStudent(student);
		return new ResponseEntity<>(ValidationMessageUtils.addSuccessMessage("Course register successfully for this student", "106"), HttpStatus.OK);
	}
	
	@PostMapping("/{studentId}/unRegesiter/courses/{courseId}")
	public ResponseEntity<ValidationMessage> unRegisterCourse(@PathVariable("studentId") Long studentId, @PathVariable("courseId") Long courseId) {
		Student student = studentService.getStudent(studentId);
		if(student == null)
			return new ResponseEntity<>(ValidationMessageUtils.addErrorMessage("student with the specified id not found in the database", "507"), HttpStatus.OK);
		
		Course course = courseService.getCourse(courseId);
		if(course == null)
			return new ResponseEntity<>(ValidationMessageUtils.addErrorMessage("course with the specified id not found in the database", "507"), HttpStatus.OK);
	
		if(!student.getCourses().contains(course)) 
			return new ResponseEntity<>(ValidationMessageUtils.addErrorMessage("Course is not registerd for this student", "506"), HttpStatus.OK);
		
		student.getCourses().remove(new Course(courseId));
		studentService.saveStudent(student);
		return new ResponseEntity<>(ValidationMessageUtils.addSuccessMessage("Course Un Register successfully for this student", "106"), HttpStatus.OK);
	}
}

