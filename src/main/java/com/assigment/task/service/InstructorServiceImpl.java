package com.assigment.task.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.assigment.task.dao.InstructorDao;
import com.assigment.task.model.Instructor;

@Service("instructorService")
@Transactional
public class InstructorServiceImpl implements InstructorService {

	private static final long serialVersionUID = -2635602094758733029L;
	
	@Autowired
	private InstructorDao instructorDao;
	
	@Override
	public void saveInstructor(Instructor instructor) {
		instructorDao.save(instructor);
	}
	
	@Override
	public Instructor getInstructor(Long id) {
		Optional<Instructor> instructor = instructorDao.findById(id);
		return instructor.isPresent() ? instructor.get() : null;
	}
}
