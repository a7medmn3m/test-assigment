package com.assigment.task.service;

import java.io.Serializable;

import com.assigment.task.model.User;

public interface UserService extends Serializable {

	User getUserByEmail(String email);

}
