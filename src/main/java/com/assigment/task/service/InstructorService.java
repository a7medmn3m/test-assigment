package com.assigment.task.service;

import java.io.Serializable;

import com.assigment.task.model.Instructor;

public interface InstructorService extends Serializable {

	void saveInstructor(Instructor instructor);

	Instructor getInstructor(Long id);

}
