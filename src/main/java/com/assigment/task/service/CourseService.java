package com.assigment.task.service;

import java.io.Serializable;
import java.util.List;

import com.assigment.task.model.Course;

public interface CourseService extends Serializable {

	List<Course> listCourses();

	void saveCourse(Course course);

	List<Course> getStudentCourses(Long studentId);

	Course getCourse(Long id);

}
