package com.assigment.task.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.assigment.task.dao.CourseDao;
import com.assigment.task.model.Course;

@Service("courseService")
@Transactional
public class CourseServiceImpl implements CourseService {

	private static final long serialVersionUID = -5252384607595873155L;
	
	@Autowired
	private CourseDao courseDao;
	
	
	@Override
	public void saveCourse(Course course) {
		courseDao.save(course);
	}
	
	@Override
	public List<Course> listCourses() {
		return (List<Course>) courseDao.findAll();
	}
	
	@Override
	public List<Course> getStudentCourses(Long studentId) {
		return courseDao.getStudentCourses(studentId);
	}
	
	@Override
	public Course getCourse(Long id) {
		Optional<Course> optional = courseDao.findById(id);
		return optional.isPresent() ? optional.get() : null;
	}
	
}
