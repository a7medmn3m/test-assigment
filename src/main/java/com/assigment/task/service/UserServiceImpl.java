package com.assigment.task.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assigment.task.dao.UserDao;
import com.assigment.task.model.User;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	private static final long serialVersionUID = -6354795684053409523L;
	
	@Autowired
	private UserDao userDao;
	
	@Override
	public User getUserByEmail(String email) {
		return userDao.findByEmail(email);
	}
}
