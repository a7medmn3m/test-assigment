package com.assigment.task.service;

import java.io.Serializable;
import java.util.List;

import com.assigment.task.model.Student;

public interface StudentService extends Serializable {

	List<Student> listStudents();

	void saveStudent(Student student);

	Student getStudent(String username, String email);

	Student getStudent(String email);

	Student getStudent(Long id);

}
