package com.assigment.task.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.assigment.task.dao.StudentDao;
import com.assigment.task.model.Student;

@Service("studentService")
@Transactional
public class StudentServiceImpl implements StudentService {

	private static final long serialVersionUID = 425141586514410395L;
	
	@Autowired
	private StudentDao studentDao;
	
	@Override
	public void saveStudent(Student student) {
		studentDao.save(student);
	}
	
	@Override
	public Student getStudent(Long id) {
		Optional<Student> optional = studentDao.findById(id);
		return optional.isPresent() ? optional.get() : null;
	}
	
	@Override
	public Student getStudent(String username, String email) {
		return studentDao.findByUsernameOrEmail(username, email);
	}
	
	@Override
	public Student getStudent(String email) {
		return studentDao.findByEmail(email);
	}
	
	@Override
	public List<Student> listStudents() {
		return (List<Student>) studentDao.findAll();
	}
}
