package com.assigment.task.model;

import com.assigment.task.enums.MessageType;

public class ValidationMessage {
	private String code;
	private String message;
	private MessageType type;
	
	public ValidationMessage(String code, String message, MessageType type) {
		super();
		this.code = code;
		this.message = message;
		this.type = type;
	}
	public ValidationMessage() {
		super();
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public MessageType getType() {
		return type;
	}
	public void setType(MessageType type) {
		this.type = type;
	}
}
