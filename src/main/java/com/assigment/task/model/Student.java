package com.assigment.task.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Student extends User {

	private static final long serialVersionUID = -1227428266480077051L;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "Student_Courses", joinColumns = {
			@JoinColumn(name = "Student_id", nullable = false, updatable = false)},
			inverseJoinColumns = { @JoinColumn(name = "Course_id", nullable = false, updatable = false) }
	)
	private List<Course> courses = new ArrayList<>();

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}
	
}
