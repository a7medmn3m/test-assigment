package com.assigment.task.utils;

import com.assigment.task.enums.MessageType;
import com.assigment.task.model.ValidationMessage;

public abstract class ValidationMessageUtils {
	
	public static ValidationMessage addSuccessMessage(String message, String code) {
		return new ValidationMessage(code, message, MessageType.SUCCESS);
	}
	
	public static ValidationMessage addErrorMessage(String message, String code) {
		return new ValidationMessage(code, message, MessageType.ERROR);
	}
}
