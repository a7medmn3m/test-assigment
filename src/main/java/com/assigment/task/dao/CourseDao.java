package com.assigment.task.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.assigment.task.model.Course;

@Repository
public interface CourseDao extends CrudRepository<Course, Long> {

	@Query(nativeQuery = true, value = "SELECT c.* FROM course c JOIN Student_Courses cs ON c.id = cs.Course_id JOIN user s ON s.id = cs.Student_id WHERE s.id = :id")
	public List<Course> getStudentCourses(@Param("id") Long studentId);
	
}
