package com.assigment.task.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.assigment.task.model.User;

@Repository
public interface UserDao extends CrudRepository<User, Long> {
	
	User findByEmail(String email);
	
}
