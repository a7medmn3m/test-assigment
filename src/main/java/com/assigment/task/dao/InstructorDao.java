package com.assigment.task.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.assigment.task.model.Instructor;

@Repository
public interface InstructorDao extends CrudRepository<Instructor, Long> {

}
