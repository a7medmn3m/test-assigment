package com.assigment.task.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.assigment.task.model.Student;

@Repository
public interface StudentDao extends CrudRepository<Student, Long> {
	
	public Student findByUsernameOrEmail(String username, String email);
	
	public Student findByEmail(String email);

}
