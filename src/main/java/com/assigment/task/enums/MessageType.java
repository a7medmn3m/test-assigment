package com.assigment.task.enums;

public enum MessageType {
		SUCCESS,
		WARN,
		INFO,
		ERROR
}
