
DROP SCHEMA IF EXISTS `assignmentDB` ;

-- -----------------------------------------------------
-- Schema assignmentDB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `assignmentDB`;
USE `assignmentDB` ;

-- -----------------------------------------------------
-- Table `assignmentDB`.`user`
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `assignmentDB`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `assignmentDB`.`user` ;

CREATE TABLE IF NOT EXISTS `assignmentDB`.`user` (
  `DTYPE` VARCHAR(31) NOT NULL,
  `id` BIGINT(20) NOT NULL,
  `creationDate` DATETIME NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `birthDate` DATETIME NULL DEFAULT NULL,
  `email` VARCHAR(255) NULL DEFAULT NULL,
  `gender` VARCHAR(255) NULL DEFAULT NULL,
  `password` VARCHAR(255) NULL DEFAULT NULL,
  `username` VARCHAR(255) NULL DEFAULT NULL,
  `createdBy_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FKi5dptbglpe43x2f5rxec9v7fh` (`createdBy_id` ASC) ,
  CONSTRAINT `FKi5dptbglpe43x2f5rxec9v7fh`
    FOREIGN KEY (`createdBy_id`)
    REFERENCES `assignmentDB`.`user` (`id`));


-- -----------------------------------------------------
-- Table `assignmentDB`.`course`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `assignmentDB`.`course` ;

CREATE TABLE IF NOT EXISTS `assignmentDB`.`course` (
  `id` BIGINT(20) NOT NULL,
  `creationDate` DATETIME NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `lastUpdated` DATETIME NULL DEFAULT NULL,
  `publishDate` DATETIME NULL DEFAULT NULL,
  `totalHours` BIGINT(20) NULL DEFAULT NULL,
  `createdBy_id` BIGINT(20) NULL DEFAULT NULL,
  `instructor_id` BIGINT(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `FKnk6kil2fs9xdft0xjvhssu20h` (`createdBy_id` ASC) ,
  INDEX `FKe3hpighs6mm740oc1pk7v8g0h` (`instructor_id` ASC) ,
  CONSTRAINT `FKe3hpighs6mm740oc1pk7v8g0h`
    FOREIGN KEY (`instructor_id`)
    REFERENCES `assignmentDB`.`user` (`id`),
  CONSTRAINT `FKnk6kil2fs9xdft0xjvhssu20h`
    FOREIGN KEY (`createdBy_id`)
    REFERENCES `assignmentDB`.`user` (`id`));


-- -----------------------------------------------------
-- Table `assignmentDB`.`hibernate_sequence`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `assignmentDB`.`hibernate_sequence` ;

CREATE TABLE IF NOT EXISTS `assignmentDB`.`hibernate_sequence` (
  `next_val` BIGINT(20) NULL DEFAULT NULL);


-- -----------------------------------------------------
-- Table `assignmentDB`.`student_courses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `assignmentDB`.`student_courses` ;

CREATE TABLE IF NOT EXISTS `assignmentDB`.`student_courses` (
  `Student_id` BIGINT(20) NOT NULL,
  `Course_id` BIGINT(20) NOT NULL,
  INDEX `FK4byinnomhua6ofig0g9hlvgo4` (`Course_id` ASC) ,
  INDEX `FK46y90gctj06y5r8v4sjb6852k` (`Student_id` ASC) ,
  CONSTRAINT `FK46y90gctj06y5r8v4sjb6852k`
    FOREIGN KEY (`Student_id`)
    REFERENCES `assignmentDB`.`user` (`id`),
  CONSTRAINT `FK4byinnomhua6ofig0g9hlvgo4`
    FOREIGN KEY (`Course_id`)
    REFERENCES `assignmentDB`.`course` (`id`));

    
    
    DROP TABLE IF EXISTS `assignmentDB`.`flyway_schema_history`;
CREATE TABLE `assignmentDB`.`flyway_schema_history` (
    `installed_rank` int(11) NOT NULL,
    `version` varchar(50) DEFAULT NULL,
    `description` varchar(200) NOT NULL,
    `type` varchar(20) NOT NULL,
    `script` varchar(1000) NOT NULL,
    `checksum` int(11) DEFAULT NULL,
    `installed_by` varchar(100) NOT NULL,
    `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `execution_time` int(11) NOT NULL,
    `success` tinyint(1) NOT NULL,
    PRIMARY KEY (`installed_rank`),
    KEY `flyway_schema_history_s_idx` (`success`)
) ;


